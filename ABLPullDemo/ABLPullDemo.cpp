﻿// rtspDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "live555Client.h"
#include "rapidjson\document.h"

#define  MaxRtspCount   512 

char  ABL_szCurrentPath[256] = { 0 };
int   rtspLoop = 1, LoopTimer = 4, printCount = 100;
char  szRtspURLArray[MaxRtspCount][96] = { 0 };
int   nRtspChan[MaxRtspCount] = { 0 };
int   nRtspURLCount = 0;

//#define  WriteMyMp4FileFlag    1 //

#ifdef WriteRtspDataFlag
MediaDataHead mediaHead;
FILE* fMediaFile;
#endif

#ifdef WriteMyMp4FileFlag
FILE* fMyFile;
unsigned char SPSPPSBuffer[1024];
int           nSPSPPSLength = 0;
bool          bRecordSPSPPSFlag = false;
#endif


//获取当前路径
BOOL GetCurrentPath(char* szCurPath)
{
	char    szPath[255] = { 0 };
	string  strTemp;
	int     nPos;

	GetModuleFileNameA(NULL, szPath, sizeof(szPath));
	strTemp = szPath;

	nPos = strTemp.rfind("\\", strlen(szPath));
	if (nPos >= 0)
	{
		memcpy(szCurPath, szPath, nPos + 1);
		return TRUE;
	}
	else
		return FALSE;
}

//音频，视频回调函数
int nRecvCount = 0;
void CALLBACK live555RTSP_AudioVideo(int nRtspChan, int RtspDataType, char* codeName, unsigned char* pAVData, int nAVDataLength, int64_t timeValue, void* pCustomerData)
{
	nRecvCount++;
	if (nRecvCount % printCount == 0)
	{
		printf("nRtspChan = %d,  RtspDataType =%d,  codeName = %s, nAVDataLength = %d \r\n", nRtspChan, RtspDataType, codeName, nAVDataLength);
	}

	if (RtspDataType == XHRtspDataType_Message)
	{//收到消息通知 
		if (strcmp(codeName, "success") == 0)
		{//连接成功，会返回媒体信息  {"video":"H264","audio":"AAC","channels":1,"sampleRate":16000} ，可以确定媒体格式 
			printf("%s \r\n", pAVData);
		}
		else if (strcmp(codeName, "error") == 0)
		{//连接失败
			printf("%s \r\n", pAVData);
		}
	}
	else if (RtspDataType == XHRtspDataType_Video)
	{//收到回调视频
		//可以进行解码、或者保存数据
	}
	else if (RtspDataType == XHRtspDataType_Audio)
	{//收到回调音频
	   //可以进行解码、或者保存数据
	}

#ifdef WriteMyMp4FileFlag
	if (pAVData[4] == 0x67)
	{//记录SPS PPS 
		if (bRecordSPSPPSFlag == false)
		{
			memcpy(SPSPPSBuffer, pAVData, nAVDataLength);
			nSPSPPSLength = nAVDataLength;
			bRecordSPSPPSFlag = true;
		}
	}


	if (RtspDataType == XHRtspDataType_Video)
	{
		int nLengthCount;
		if (pAVData[4] == 0x65)
		{
			nLengthCount = nSPSPPSLength + nAVDataLength;
			fwrite((char*)&nLengthCount, 1, sizeof(int), fMyFile);
			fwrite((char*)SPSPPSBuffer, 1, nSPSPPSLength, fMyFile);

			fwrite((char*)pAVData, 1, nAVDataLength, fMyFile);
			fflush(fMyFile);
		}
	}
#endif

#ifdef WriteRtspDataFlag
	if (RtspDataType == XHRtspDataType_Video || RtspDataType == XHRtspDataType_Audio)
	{
		mediaHead.mediaDataLength = nAVDataLength;
		if (RtspDataType == XHRtspDataType_Video)
		{
			if (strcmp(codeName, "H264") == 0)
				mediaHead.mediaDataType = MedisDataType_H264;
			else if (strcmp(codeName, "H265") == 0)
				mediaHead.mediaDataType = MedisDataType_H265;
		}
		else if (RtspDataType == XHRtspDataType_Audio)
		{
			if (strcmp(codeName, "G711_A") == 0)
				mediaHead.mediaDataType = MedisDataType_G711A;
			else if (strcmp(codeName, "G711_U") == 0)
				mediaHead.mediaDataType = MedisDataType_G711U;
			else if (strcmp(codeName, "AAC") == 0)
				mediaHead.mediaDataType = MedisDataType_AAC;
		}

		fwrite((char*)&mediaHead, 1, sizeof(mediaHead), fMediaFile);
		fwrite(pAVData, 1, nAVDataLength, fMediaFile);
		fflush(fMediaFile);
	}
#endif
}

bool  bExitRtspThreadFlag = true;
bool  bRunFlag = false;
int CALLBACK OnRtspThread(LPVOID lpVoid)
{
	int i;
	bExitRtspThreadFlag = false;
	bRunFlag = true;
	while (bRunFlag)
	{
		for (i = 0; i < nRtspURLCount; i++)
		{
			live555Client_ConnectCallBack(szRtspURLArray[i], XHRtspURLType_Liveing, true, (void*)NULL, live555RTSP_AudioVideo, nRtspChan[i]);
			Sleep(200);
		}

		Sleep(1000 * LoopTimer);

		for (i = 0; i < nRtspURLCount; i++)
			live555Client_Disconnect(nRtspChan[i]);
	}
	bExitRtspThreadFlag = true;

	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	char* szJonsFileBuffer = new char[1024 * 1024];
	FILE* fFile;
	char  szFileName[256] = { 0 };

#if   0
	string strJsonTest = "{\"item_1\":\"value_1\",\"item_2\":\"value_2\",\"item_3\":\"value_3\",\"item_4\":\"value_4\",\"item_arr\":[\"arr_vaule_1\",\"arr_vaule_2\"]}";
	Document docTest;
	docTest.Parse<0>(strJsonTest.c_str());

	if (!docTest.HasParseError())
	{
		for (rapidjson::Value::ConstMemberIterator itr = docTest.MemberBegin(); itr != docTest.MemberEnd(); itr++)
		{
			Value jKey;
			Value jValue;
			Document::AllocatorType allocator;
			jKey.CopyFrom(itr->name, allocator);
			jValue.CopyFrom(itr->value, allocator);
			if (jKey.IsString())
			{
				string name = jKey.GetString();
				string value = jValue.GetString();
				printf("name: %s value: %s \r\n", name.c_str(), value.c_str());
			}
		}
	}

#endif

	GetCurrentPath(ABL_szCurrentPath);
	sprintf(szFileName, "%s%s", ABL_szCurrentPath, "rtspURL.json");
	fFile = fopen(szFileName, "rb");
	if (fFile == NULL)
		return -1;

	memset(szJonsFileBuffer, 0x00, 1024 * 1024);
	fread(szJonsFileBuffer, 1, 48 * 1024, fFile);
	fclose(fFile);

	rapidjson::Document doc;
	doc.Parse<0>((char*)szJonsFileBuffer);
	int listSize;
	if (!doc.HasParseError())
	{
		rtspLoop = doc["rtspLoop"].GetInt64();
		LoopTimer = doc["LoopTimer"].GetInt64();
		if (doc["PrintCount"].IsNull() == false)
			printCount = doc["PrintCount"].GetInt64();

		listSize = doc["rtspURL"].Size();
		for (int i = 0; i < listSize; i++)
		{
			strcpy(szRtspURLArray[i], doc["rtspURL"][i].GetString());
			if (i >= MaxRtspCount)
				break;
			nRtspURLCount++;
		}
	}
	if (nRtspURLCount == 0)
	{
		delete[] szJonsFileBuffer;
		szJonsFileBuffer = NULL;

		printf("Can't Find RtspURL \r\n");
		return -1;
	}

	live555Client_Init(NULL);

#ifdef WriteRtspDataFlag
	fMediaFile = fopen("F:\\mediaFile2020-10-27.myMP4", "wb");
#endif

#ifdef WriteMyMp4FileFlag
	fMyFile = fopen("E:\\recv_2021-07-20.my264", "wb");
#endif

	if (rtspLoop == 0)
	{//不循环
		for (int i = 0; i < nRtspURLCount; i++)
		{
			live555Client_ConnectCallBack(szRtspURLArray[i], XHRtspURLType_Liveing, true, (void*)NULL, live555RTSP_AudioVideo, nRtspChan[i]);
		}
	}
	else
	{//循环 
		DWORD dwThread;
		::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)OnRtspThread, (LPVOID)NULL, 0, &dwThread);
	}

	unsigned char nGetChar;
	while (true)
	{
		nGetChar = getchar();
		if (nGetChar == 'x')
		{
			break;
		}
	}

	delete[] szJonsFileBuffer;
	szJonsFileBuffer = NULL;

	bRunFlag = false;
	while (!bExitRtspThreadFlag)
		Sleep(100);

	for (int i = 0; i < nRtspURLCount; i++)
	{
		live555Client_Disconnect(nRtspChan[i]);
		Sleep(150);
	}

	live555Client_Cleanup();

#ifdef WriteRtspDataFlag
	fflush(fMediaFile);
	fclose(fMediaFile);
#endif

#ifdef WriteMyMp4FileFlag
	fclose(fMyFile);
#endif

	return 0;
}

