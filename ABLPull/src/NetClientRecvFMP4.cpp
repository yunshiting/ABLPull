/*
功能：
    实现FMP4客户端的接收模块   

*/

#include "stdafx.h"
#include "NetClientRecvFMP4.h"

extern bool                                  DeleteNetRevcBaseClient(NETHANDLE CltHandle);

extern CMediaFifo                            pDisconnectBaseNetFifo; //清理断裂的链接 
extern int                                   avpriv_mpeg4audio_sample_rates[] ;
extern char                                  ABL_MediaSeverRunPath[256]; //当前路径

extern void LIBNET_CALLMETHOD	onconnect(NETHANDLE clihandle,
	uint8_t result);

extern void LIBNET_CALLMETHOD onread(NETHANDLE srvhandle,
	NETHANDLE clihandle,
	uint8_t* data,
	uint32_t datasize,
	void* address);

extern void LIBNET_CALLMETHOD	onclose(NETHANDLE srvhandle,
	NETHANDLE clihandle);

CNetClientRecvFMP4::CNetClientRecvFMP4(NETHANDLE hServer, NETHANDLE hClient, char* szIP, unsigned short nPort,char* szShareMediaURL, void* pCustomerPtr, LIVE555RTSP_AudioVideo callbackFunc, uint64_t hParent, int nXHRtspURLType)
{
	cbMediaCodecNameFlag = false;
	bCheckRtspVersionFlag = false;
	bDeleteRtmpPushH265Flag = false;
	nServer = hServer;
	nClient = hClient;
	strcpy(szClientIP, szIP);
	nClientPort = nPort;
	strcpy(m_szShareMediaURL, szShareMediaURL);

	m_nXHRtspURLType = nXHRtspURLType;
	m_pCustomerPtr = pCustomerPtr;
	m_callbackFunc = callbackFunc;
	m_hParent = hParent;
	
	netDataCache = new unsigned char[HttpFlvReadPacketSize]; //网络数据缓存

	nWriteRet = 0;
	nWriteErrorCount = 0;

	if (ParseRtspRtmpHttpURL(szIP) == true)
		uint32_t ret = XHNetSDK_Connect((int8_t*)m_rtspStruct.szIP, atoi(m_rtspStruct.szPort), (int8_t*)(NULL), 0, (uint64_t*)&nClient, onread, onclose, onconnect, 0, 5000, 1);

	nVideoDTS = 0;
	nAudioDTS = 0;
	memset(szRtmpName, 0x00, sizeof(szRtmpName));
	netDataCacheLength = nNetStart = nNetEnd = 0;
	netBaseNetType = NetBaseNetType_HttpMP4ClientRecv;

	WriteLog(Log_Debug, "CNetClientRecvFMP4 构造 = %X nClient = %llu ", this, nClient);
}

CNetClientRecvFMP4::~CNetClientRecvFMP4()
{
	std::lock_guard<std::mutex> lock(NetClientRecvMP4VLock);

	XHNetSDK_Disconnect(nClient);

#ifdef  SaveNetDataToMP4File
	if (fileMP4 != NULL)
		fclose(fileMP4);
#endif

	 SAFE_ARRAY_DELETE(netDataCache);

	WriteLog(Log_Debug, "CNetClientRecvFMP4 析构 = %X nClient = %llu \r\n", this, nClient);
}

int CNetClientRecvFMP4::PushVideo(uint8_t* pVideoData, uint32_t nDataLength, char* szVideoCodec)
{
	return 0;
}

int CNetClientRecvFMP4::PushAudio(uint8_t* pVideoData, uint32_t nDataLength, char* szAudioCodec, int nChannels, int SampleRate)
{
	return 0;
}
int CNetClientRecvFMP4::SendVideo()
{
	return 0;
}

int CNetClientRecvFMP4::SendAudio()
{

	return 0;
}

int CNetClientRecvFMP4::InputNetData(NETHANDLE nServerHandle, NETHANDLE nClientHandle, uint8_t* pData, uint32_t nDataLength)
{
	std::lock_guard<std::mutex> lock(NetClientRecvMP4VLock);

	//网络断线检测
	nRecvDataTimerBySecond = 0;
 
	if (bRecvHttp200OKFlag == false)
	{//去掉http回复的包头
		unsigned char szHttpEndFlag[4] = { 0x0d,0x0a,0x0d,0x0a };
		int nPos = 0;
		for (int i = 0; i < nDataLength; i++)
		{
			if (memcmp(pData + i, szHttpEndFlag, 4) == 0)
			{
				nPos = i;
				break;
			}
		}
 		if (nPos > 0)
		{
			bRecvHttp200OKFlag = true;
 			if (nDataLength - (nPos + 4) > 0)
			{
				memcpy(netDataCache + nNetEnd, pData+(nDataLength - (nPos + 4)), nDataLength - (nPos + 4));
				netDataCacheLength  += nDataLength - (nPos + 4);
				nNetEnd            += nDataLength - (nPos + 4);
			}
			else
				return 0;
		}
	}

	if (HttpFlvReadPacketSize - nNetEnd >= nDataLength)
	{//剩余空间足够
		memcpy(netDataCache + nNetEnd, pData, nDataLength);
		netDataCacheLength += nDataLength;
		nNetEnd += nDataLength;
	}
	else
	{//剩余空间不够，需要把剩余的buffer往前移动
		if (netDataCacheLength > 0)
		{//如果有少量剩余
			memmove(netDataCache, netDataCache + nNetStart, netDataCacheLength);
			nNetStart = 0;
			nNetEnd = netDataCacheLength;

			if (HttpFlvReadPacketSize - nNetEnd < nDataLength)
			{
				nNetStart = nNetEnd = netDataCacheLength = 0;
				WriteLog(Log_Debug, "CNetClientRecvFMP4 = %X nClient = %llu 数据异常！执行删除", this, nClient);
				pDisconnectBaseNetFifo.push((unsigned char*)&nClient,sizeof(nClient));
				return 0;
 			}
		}
		else
		{//没有剩余，那么 首，尾指针都要复位 
			nNetStart = nNetEnd = netDataCacheLength = 0;
		}
		memcpy(netDataCache + nNetEnd, pData, nDataLength);
		netDataCacheLength += nDataLength;
		nNetEnd += nDataLength;
	}

    return 0;
}

int CNetClientRecvFMP4::ProcessNetData()
{
	std::lock_guard<std::mutex> lock(NetClientRecvMP4VLock);

	//test 	消费，使用接收到的缓存 
	nNetStart += netDataCacheLength;
	netDataCacheLength = 0;

#ifdef  SaveNetDataToMP4File
	 
	if (fileMP4 && netDataCacheLength > 0)
	{
		fwrite(netDataCache + nNetStart, 1, netDataCacheLength, fileMP4);
		fflush(fileMP4);

		nNetStart += netDataCacheLength;
		netDataCacheLength = 0;
	}
#endif
	return 0;
}

//发送第一个请求
int CNetClientRecvFMP4::SendFirstRequst()
{
	string  strHttpFlvURL = m_rtspStruct.szSrcRtspPullUrl;
	int nPos1, nPos2;
	char    szSubPath[256] = { 0 };
	nPos1 = strHttpFlvURL.find("//", 0);
	if (nPos1 > 0)
	{
		nPos2 = strHttpFlvURL.find("/", nPos1 + 2);
		if (nPos2 > 0)
		{
			memcpy(szSubPath, m_rtspStruct.szSrcRtspPullUrl + nPos2, strlen(m_rtspStruct.szSrcRtspPullUrl) - nPos2);
			sprintf(szRequestMP4File, "GET %s HTTP/1.1\r\nUser-Agent: %s\r\nAccept: */*\r\nRange: bytes=0-\r\nConnection: keep-alive\r\nHost: 190.15.240.11:8088\r\nIcy-MetaData: 1\r\n\r\n", szSubPath, MediaServerVerson);
			XHNetSDK_Write(nClient, (unsigned char*)szRequestMP4File, strlen(szRequestMP4File), 1);
		}else
			pDisconnectBaseNetFifo.push((unsigned char*)nClient, sizeof(nClient));
	}else
		pDisconnectBaseNetFifo.push((unsigned char*)nClient, sizeof(nClient));

#ifdef  SaveNetDataToMP4File
	sprintf(szRequestMP4File, "%s%X.mp4", ABL_MediaSeverRunPath, this);
	fileMP4 = fopen(szRequestMP4File, "wb"); ;
#endif

	return 0;
}

//请求m3u8文件
bool  CNetClientRecvFMP4::RequestM3u8File()
{
	return true;
}