#ifndef _NetClientRecvFMP4_H
#define _NetClientRecvFMP4_H

#include "flv-reader.h"
#include "flv-demuxer.h"
#include "flv-muxer.h"
#include "flv-proto.h"


#ifdef USE_BOOST

#include <boost/unordered/unordered_map.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/unordered/unordered_map.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string.hpp>
using namespace boost;

#else
#include <map>
#include <queue>
#include <condition_variable> 
#include <memory>
#include <string>
#endif

//#define         SaveNetDataToMP4File          1 

#define       HttpFlvReadPacketSize     1024*1024*3

class CNetClientRecvFMP4 : public CNetRevcBase
{
public:
	CNetClientRecvFMP4(NETHANDLE hServer, NETHANDLE hClient, char* szIP, unsigned short nPort, char* szShareMediaURL, void* pCustomerPtr, LIVE555RTSP_AudioVideo callbackFunc, uint64_t hParent, int nXHRtspURLType);
   ~CNetClientRecvFMP4() ;

   std::mutex  NetClientRecvMP4VLock;
   int         type ;
   size_t      taglen;
   uint32_t    timestamp;

   volatile  bool bRecvHttp200OKFlag;
   uint32_t       nVideoDTS ;
   uint32_t       nAudioDTS;
   uint32_t       nWriteRet;
   volatile  int  nWriteErrorCount;
   char           szRtmpName[512];
   unsigned char*  packet;
   unsigned char*  netDataCache; //网络数据缓存
   int             netDataCacheLength;//网络数据缓存大小
   int             nNetStart, nNetEnd; //网络数据起始位置\结束位置

   virtual int InputNetData(NETHANDLE nServerHandle, NETHANDLE nClientHandle, uint8_t* pData, uint32_t nDataLength) ;
   virtual int ProcessNetData();

   virtual int PushVideo(uint8_t* pVideoData, uint32_t nDataLength, char* szVideoCodec) ;//塞入视频数据
   virtual int PushAudio(uint8_t* pVideoData, uint32_t nDataLength, char* szAudioCodec, int nChannels, int SampleRate) ;//塞入音频数据
   virtual int SendVideo();//发送视频数据
   virtual int SendAudio();//发送音频数据
   virtual int SendFirstRequst();//发送第一个请求
   virtual bool RequestM3u8File();//请求m3u8文件

   volatile bool                bCheckRtspVersionFlag;
   char                         szURL[512];
   char                         szRequestMP4File[512];

   volatile bool                bDeleteRtmpPushH265Flag; //因为推rtmp265被删除标志 

#ifdef  SaveNetDataToMP4File
   FILE*                        fileMP4;
   int64_t                      nNetPacketNumber;
#endif
};

#endif