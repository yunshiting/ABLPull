#ifndef _NetBaseThreadPool_H
#define _NetBaseThreadPool_H


#include <condition_variable> 


#ifdef USE_BOOST
#include <boost/lockfree/queue.hpp>
#include <condition_variable> 
#include <boost/unordered/unordered_map.hpp>
#else
#include <map>
#include <queue>
#include <condition_variable> 

#endif



#define   MaxNetHandleQueueCount    512 

#ifdef USE_BOOST
typedef boost::unordered_map<NETHANDLE, NETHANDLE>   ClientProcessThreadMap;//固定客户端的线程序号 
#else
typedef std::map<NETHANDLE, NETHANDLE>   ClientProcessThreadMap;//固定客户端的线程序号 

#endif


class CNetBaseThreadPool
{
public:
	CNetBaseThreadPool(int nThreadCount);
   ~CNetBaseThreadPool();

   //插入任务ID 
   bool  InsertIntoTask(uint64_t nClientID);
   void  StopTask();

   CRITICAL_SECTION pThreadPollLock;
private:
	volatile int nGetCurrentThreadOrder;
	void ProcessFunc();
	static UINT OnProcessThread(LPVOID lpVoid);

	volatile   uint64_t     nThreadProcessCount;
	std::mutex              threadLock;
	ClientProcessThreadMap  clientThreadMap;
    uint64_t              nTrueNetThreadPoolCount; 

    volatile bool         bExitProcessThreadFlag[MaxNetHandleQueueCount];
    volatile bool         bCreateThreadFlag;
    HANDLE                hProcessHandle[MaxNetHandleQueueCount];
	volatile  bool        bRunFlag;
	std::condition_variable  cv[MaxNetHandleQueueCount];
	std::mutex               mtx[MaxNetHandleQueueCount];

#ifdef USE_BOOST
	boost::lockfree::queue<uint64_t, boost::lockfree::capacity<2048>> m_NetHandleQueue[MaxNetHandleQueueCount];
#else
	std::queue<uint64_t> m_NetHandleQueue[MaxNetHandleQueueCount];
#endif

};

#endif