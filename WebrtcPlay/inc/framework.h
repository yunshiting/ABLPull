﻿#pragma once



#define WIN32_LEAN_AND_MEAN             // 从 Windows 头文件中排除极少使用的内容
// Windows 头文件
#include <windows.h>
#include <shellapi.h>  // must come after windows.h
// C 运行时头文件
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include "XHNetSDK.h"
#include "ABLSipParse.h"
#include "ABLogSDK.h"

#include "NetRecvBase.h"
#include "NetBaseThreadPool.h"
#include "NetClientAddStreamProxy.h"
#include "NetClientRecvRtsp.h"
#include "NetClientRecvFLV.h"
#include "NetClientRecvRtmp.h"
#include "NetClientRecvHttpHLS.h"
#include "NetClientRecvFMP4.h"
#include "ReadRecordFileInput.h"

#ifdef USE_BOOST
#include <boost/unordered/unordered_map.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/unordered/unordered_map.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
using namespace boost;
#else

#include "ABLString.h"
#endif



#pragma comment(lib,"version.lib")
#pragma comment(lib,"Imm32.lib")
#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"advapi32.lib")
#pragma comment(lib,"comdlg32.lib")
#pragma comment(lib,"dbghelp.lib")
#pragma comment(lib,"dnsapi.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"msimg32.lib")
#pragma comment(lib,"odbc32.lib")
#pragma comment(lib,"odbccp32.lib")
#pragma comment(lib,"shell32.lib")
#pragma comment(lib,"shlwapi.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"usp10.lib")
#pragma comment(lib,"uuid.lib")
#pragma comment(lib,"wininet.lib")
#pragma comment(lib,"winmm.lib")
#pragma comment(lib,"winspool.lib")
#pragma comment(lib,"delayimp.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"ole32.lib")
#pragma comment(lib,"crypt32.lib")
#pragma comment(lib,"iphlpapi.lib")
#pragma comment(lib,"secur32.lib")
#pragma comment(lib,"dmoguids.lib")
#pragma comment(lib,"wmcodecdspuuid.lib")
#pragma comment(lib,"amstrmid.lib")
#pragma comment(lib,"msdmo.lib")
#pragma comment(lib,"strmiids.lib")
#pragma comment(lib,"oleaut32.lib")
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"dwmapi.lib")




#ifndef WIN64

#ifdef _DEBUG

#pragma comment(lib, "../Lib/debug/webrtc.lib")


#else
#pragma comment(lib, "../SDK/lib/XHNetSDK.lib")
#pragma comment(lib, "../SDK/lib/PsDemux.lib")
#pragma comment(lib, "../SDK/lib/PsMux.lib")
#pragma comment(lib, "../SDK/lib/rtppacket.lib")
#pragma comment(lib, "../SDK/lib/rtpdepacket.lib")
#pragma comment(lib, "../3rd/media-server-master/x64/Release/librtmp.lib")
#pragma comment(lib, "../3rd/media-server-master/x64/Release/libflv.lib")
#pragma comment(lib, "../3rd/media-server-master/x64/Release/libmpeg.lib")
#pragma comment(lib, "../3rd/media-server-master/x64/Release/libmov.lib")
#pragma comment(lib, "../webrtc/lib64/release/webrtc.lib")

#endif

#else

#ifdef _DEBUG

#pragma comment(lib, "../Lib64/debug/webrtc.lib")

#else

#pragma comment(lib, "../Lib64/release/webrtc.lib")
#pragma comment(lib, "../Lib64/release/rtc_base.lib")

//#pragma comment(lib, "../Lib64/release/librtmp.lib")
#pragma comment(lib,"../Lib64/libfaac.lib")
#pragma comment(lib,"../Lib64/libx264.lib")
//#pragma comment(lib, "../Lib64/release/fdk-aac.dll.lib")
#pragma comment(lib, "../Lib64/release/fdk-aac.lib")
#endif






#endif// LIBUS_NO_SSL
