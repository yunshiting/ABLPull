/*
功能：
    自研的线程池功能，能以多个线程去执行一类实例，不同的任务
    这个线程池，当网络数据达到时触发线程执行 	
日期    2021-03-29
作者    罗家兄弟
QQ      79941308
E-Mail  79941308@qq.com
*/

#include "stdafx.h"
#include "NetBaseThreadPool.h"
#include "NetRecvBase.h"

extern std::shared_ptr<CNetRevcBase> GetNetRevcBaseClient(NETHANDLE CltHandle);

CNetBaseThreadPool::CNetBaseThreadPool(int nThreadCount)
{
	nThreadProcessCount = 0;
 	nTrueNetThreadPoolCount = nThreadCount;
	if (nThreadCount > MaxNetHandleQueueCount)
		nTrueNetThreadPoolCount = MaxNetHandleQueueCount;

	if (nThreadCount <= 0)
		nTrueNetThreadPoolCount = 64 ;

	InitializeCriticalSection(&pThreadPollLock);

	DWORD dwThread;
	bRunFlag = true;
	nGetCurrentThreadOrder = 0;
	for (int i = 0; i < nTrueNetThreadPoolCount; i++)
	{
		bCreateThreadFlag = false;
 		hProcessHandle[i] = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)OnProcessThread, (LPVOID)this, 0, &dwThread);
   		Sleep(50);
	}
	WriteLog(Log_Debug, "CNetBaseThreadPool= %X, 构造 ", this);
}

CNetBaseThreadPool::~CNetBaseThreadPool()
{
	StopTask();

	DeleteCriticalSection(&pThreadPollLock);
	WriteLog(Log_Debug, "CNetBaseThreadPool %X 析构 \r\n", this );
}

void  CNetBaseThreadPool::StopTask()
{
	if (bRunFlag)
	{
		bRunFlag = false;
		Sleep(100);

		int i;
		std::lock_guard<std::mutex> lock(threadLock);
		for (i = 0; i < nTrueNetThreadPoolCount; i++)
		{//通知所有线程
 			cv[i].notify_one();
		}

		for (int i = 0; i < nTrueNetThreadPoolCount; i++)
		{
			while (!bExitProcessThreadFlag[i])
				Sleep(50);
			CloseHandle(hProcessHandle[i]);
		}
	}
}

void CNetBaseThreadPool::ProcessFunc()
{
	EnterCriticalSection(&pThreadPollLock);
 		int nCurrentThreadID = nGetCurrentThreadOrder;
 		bExitProcessThreadFlag[nCurrentThreadID] = false;
	    bCreateThreadFlag = true; //创建线程完毕
		nGetCurrentThreadOrder ++;
	LeaveCriticalSection(&pThreadPollLock);
	uint64_t nClientID;
	WriteLog(Log_Debug, "CNetBaseThreadPool = %X nCurrentThreadID = %d ", this, nCurrentThreadID);

	bCreateThreadFlag = true; //创建线程完毕
	while (bRunFlag)
	{
#ifdef USE_BOOST
		if (m_NetHandleQueue[nCurrentThreadID].pop(nClientID) && bRunFlag)
		{
			boost::shared_ptr<CNetRevcBase> pClient = GetNetRevcBaseClient(nClientID);
			if (pClient != NULL)
			{
				pClient->ProcessNetData();//任务执行
			}
		}
		else
		{
			if (bRunFlag)
			{
				std::unique_lock<std::mutex> lck(mtx[nCurrentThreadID]);
				cv[nCurrentThreadID].wait(lck);
			}
			else
				break;
		}
#else
		if (!m_NetHandleQueue[nCurrentThreadID].empty() && bRunFlag)
		{
			nClientID = m_NetHandleQueue[nCurrentThreadID].front();
			m_NetHandleQueue[nCurrentThreadID].pop();
			auto pClient = GetNetRevcBaseClient(nClientID);
			if (pClient != NULL)
			{
				pClient->ProcessNetData();//任务执行
			}
		}
		else
		{
			if (bRunFlag)
			{
				std::unique_lock<std::mutex> lck(mtx[nCurrentThreadID]);
				cv[nCurrentThreadID].wait(lck);
			}
			else
				break;
		}
#endif
 	}
	bExitProcessThreadFlag[nCurrentThreadID] = true;
}

UINT CNetBaseThreadPool::OnProcessThread(LPVOID lpVoid)
{
	CNetBaseThreadPool* pThread = (CNetBaseThreadPool*)lpVoid;
	pThread->ProcessFunc();
	return  0;
}

bool CNetBaseThreadPool::InsertIntoTask(uint64_t nClientID)
{
	std::lock_guard<std::mutex> lock(threadLock);

	int               nThreadThread = 0;
	ClientProcessThreadMap::iterator it;

	it = clientThreadMap.find(nClientID);
	if (it != clientThreadMap.end())
	{//找到 
		nThreadThread = (*it).second;
	}
	else
	{//尚未加入过
		nThreadThread = nThreadProcessCount % nTrueNetThreadPoolCount;
 		clientThreadMap.insert(ClientProcessThreadMap::value_type(nClientID, nThreadThread));
		nThreadProcessCount  ++;
	}

	m_NetHandleQueue[nThreadThread].push(nClientID);
	cv[nThreadThread].notify_one();

	return true;
}
