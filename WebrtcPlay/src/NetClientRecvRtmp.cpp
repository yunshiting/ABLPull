/*
功能：
    实现rtmp客户端的接收模块 ， 
	 
日期    2021-07-18
作者    罗家兄弟
QQ      79941308
E-Mail  79941308@qq.com
*/

#include "stdafx.h"
#include "NetClientRecvRtmp.h"

extern bool                                  DeleteNetRevcBaseClient(NETHANDLE CltHandle);

extern CMediaFifo                            pDisconnectBaseNetFifo; //清理断裂的链接 
extern int                                   avpriv_mpeg4audio_sample_rates[] ;

extern void LIBNET_CALLMETHOD	onconnect(NETHANDLE clihandle,
	uint8_t result);

extern void LIBNET_CALLMETHOD onread(NETHANDLE srvhandle,
	NETHANDLE clihandle,
	uint8_t* data,
	uint32_t datasize,
	void* address);

extern void LIBNET_CALLMETHOD	onclose(NETHANDLE srvhandle,
	NETHANDLE clihandle);

static int rtmp_client_send(void* param, const void* header, size_t len, const void* data, size_t bytes)
{
	CNetClientRecvRtmp* pClient = (CNetClientRecvRtmp*)param;

	if (pClient != NULL && pClient->bRunFlag)
	{
		if (len > 0 && header != NULL)
		{
			pClient->nWriteRet = XHNetSDK_Write(pClient->nClient, (uint8_t*)header, len, true);
			if (pClient->nWriteRet != 0)
			{
				pClient->nWriteErrorCount++;
				WriteLog(Log_Debug, "rtmp_client_send 发送失败，次数 nWriteErrorCount = %d ", pClient->nWriteErrorCount);
			}
			else
				pClient->nWriteErrorCount = 0;
		}
		if (bytes > 0 && data != NULL)
		{
			pClient->nWriteRet = XHNetSDK_Write(pClient->nClient, (uint8_t*)data, bytes, true);
			if (pClient->nWriteRet != 0)
			{
				pClient->nWriteErrorCount ++;
				WriteLog(Log_Debug,"rtmp_client_send 发送失败，次数 nWriteErrorCount = %d ", pClient->nWriteErrorCount);
			}
			else
				pClient->nWriteErrorCount = 0;
		}
	}
	return len + bytes;
}

static int rtmp_client_onaudio(void* param, const void* data, size_t bytes, uint32_t timestamp)
{
	CNetClientRecvRtmp* pNetClientRtmp = (CNetClientRecvRtmp*)param ;
	if (pNetClientRtmp == NULL || !pNetClientRtmp->bRunFlag )
		return 0;

	flv_demuxer_input(pNetClientRtmp->flvDemuxer, FLV_TYPE_AUDIO, data, bytes, timestamp);
	return 0;
}

static int rtmp_client_onvideo(void* param, const void* data, size_t bytes, uint32_t timestamp)
{
	CNetClientRecvRtmp* pNetClientRtmp = (CNetClientRecvRtmp*)param;
	if (pNetClientRtmp == NULL || !pNetClientRtmp->bRunFlag)
		return 0;
	flv_demuxer_input(pNetClientRtmp->flvDemuxer, FLV_TYPE_VIDEO, data, bytes, timestamp);
	return 0; 
}

static int rtmp_client_onscript(void* param, const void* data, size_t bytes, uint32_t timestamp)
{
	CNetClientRecvRtmp* pNetClientRtmp = (CNetClientRecvRtmp*)param;
	if (pNetClientRtmp == NULL || !pNetClientRtmp->bRunFlag)
		return 0;
	return  flv_demuxer_input(pNetClientRtmp->flvDemuxer, FLV_TYPE_SCRIPT, data, bytes, timestamp);
}

static int NetRtmpClientRecvCallBackFLV(void* param, int codec, const void* data, size_t bytes, uint32_t pts, uint32_t dts, int flags)
{
	CNetClientRecvRtmp* pClient = (CNetClientRecvRtmp*)param;

	static char s_pts[64], s_dts[64];
	static uint32_t v_pts = 0, v_dts = 0;
	static uint32_t a_pts = 0, a_dts = 0;

	if (pClient == NULL || pClient->m_callbackFunc == NULL || !pClient->bRunFlag)
		return 0;

	if (FLV_AUDIO_AAC == codec)
	{
		if (pClient->cbMediaCodecNameFlag == false && strlen(pClient->m_mediaCodecInfo.szAudioName) == 0 && bytes > 4 && data != NULL)
		{
			unsigned char* pAudioData = (unsigned char*)data;
			strcpy(pClient->m_mediaCodecInfo.szAudioName, "AAC");

			//采样频率序号只占4位，  8 7 6 5 4 3 2 1  在 6 ~ 3 位，共4个位。所以要和0x3c 与运算，把别的位全部置为0 ，再往右移动2位，
			unsigned char nSampleIndex = ((pAudioData[2] & 0x3c) >> 2) & 0x0F;  //从 pb[2] 中获取采样频率的序号
			if (nSampleIndex <= 12)
				pClient->m_mediaCodecInfo.nSampleRate = avpriv_mpeg4audio_sample_rates[nSampleIndex];

			//通道数量计算 pAVData[2]  中有2个位，在最后2位，根 0x03 与运算，得到两位，左移动2位 ，再 或 上 pAVData[3] 的左边最高2位
			//pAVData[3] 左边最高2位获取方法 先 和 0xc0 与运算，再右移6位，为什么要右移6位？因为这2位是在最高位，所以要往右边移动6位
			pClient->m_mediaCodecInfo.nChannels = ((pAudioData[2] & 0x03) << 2) | ((pAudioData[3] & 0xc0) >> 6);
		}

		if (pClient->cbMediaCodecNameFlag == true)
			(*pClient->m_callbackFunc) (pClient->m_hParent, XHRtspDataType_Audio, pClient->m_mediaCodecInfo.szAudioName, (unsigned char*)data, bytes, pts, pClient->m_pCustomerPtr);

#ifdef  WriteFlvToEsFileFlag
		if (pClient != NULL)
		{
			fwrite(data, 1, bytes, pClient->fWriteAudio);
			fflush(pClient->fWriteAudio);
		}
#endif
		
	}
	else if (FLV_VIDEO_H264 == codec || FLV_VIDEO_H265 == codec)
	{
		if (pClient->cbMediaCodecNameFlag == false && strlen(pClient->m_mediaCodecInfo.szVideoName) == 0)
		{
			if (FLV_VIDEO_H264 == codec)
				strcpy(pClient->m_mediaCodecInfo.szVideoName, "H264");
			else if (FLV_VIDEO_H265 == codec)
				strcpy(pClient->m_mediaCodecInfo.szVideoName, "H265");
		}

		if (pClient->cbMediaCodecNameFlag == false)
		{
			pClient->nVideoFrameCount++;
			if (pClient->nVideoFrameCount >= 10 || strlen(pClient->m_mediaCodecInfo.szAudioName) > 0)
			{
				sprintf(pClient->szMediaCodecName, "{\"video\":\"%s\",\"audio\":\"%s\",\"channels\":%d,\"sampleRate\":%d}", pClient->m_mediaCodecInfo.szVideoName, pClient->m_mediaCodecInfo.szAudioName, pClient->m_mediaCodecInfo.nChannels, pClient->m_mediaCodecInfo.nSampleRate);
				(*pClient->m_callbackFunc) (pClient->m_hParent, XHRtspDataType_Message, (char*)"success", (unsigned char*)pClient->szMediaCodecName, strlen(pClient->szMediaCodecName), pts, pClient->m_pCustomerPtr);
				pClient->cbMediaCodecNameFlag = true;
				WriteLog(Log_Debug, "CNetClientRecvRtmp = %X nClient = %llu 回调媒体信息：%s ", pClient, pClient->nClient, pClient->szMediaCodecName);
			}
		}

		if (pClient->cbMediaCodecNameFlag == true)
			(*pClient->m_callbackFunc) (pClient->m_hParent, XHRtspDataType_Video, pClient->m_mediaCodecInfo.szVideoName, (unsigned char*)data, bytes, pts, pClient->m_pCustomerPtr);

#ifdef  WriteFlvToEsFileFlag
		if (pClient != NULL)
		{
			fwrite(data, 1, bytes, pClient->fWriteVideo);
			fflush(pClient->fWriteVideo);
		}
#endif
		 
	}
	else if (FLV_AUDIO_MP3 == codec)
	{
	}
	else if (FLV_AUDIO_ASC == codec || FLV_VIDEO_AVCC == codec || FLV_VIDEO_HVCC == codec)
	{
		// nothing to do
	}
	else if ((3 << 4) == codec)
	{
		//fwrite(data, bytes, 1, aac);
	}
	else
	{
		// nothing to do
		//assert(0);
	}
	return 0;
}

CNetClientRecvRtmp::CNetClientRecvRtmp(NETHANDLE hServer, NETHANDLE hClient, char* szIP, unsigned short nPort,char* szShareMediaURL, void* pCustomerPtr, LIVE555RTSP_AudioVideo callbackFunc, uint64_t hParent, int nXHRtspURLType)
{
	rtmp = NULL;
	cbMediaCodecNameFlag = false;
	bCheckRtspVersionFlag = false;
	bDeleteRtmpPushH265Flag = false;
	nServer = hServer;
	nClient = hClient;
	strcpy(szClientIP, szIP);
	nClientPort = nPort;
	NetDataFifo.InitFifo(MaxNetDataCacheBufferLength);
	strcpy(m_szShareMediaURL, szShareMediaURL);

	m_nXHRtspURLType = nXHRtspURLType;
	m_pCustomerPtr = pCustomerPtr;
	m_callbackFunc = callbackFunc;
	m_hParent = hParent;

	int r;

	flvDemuxer = NULL;
	nWriteRet = 0;
	nWriteErrorCount = 0;

#ifdef  WriteFlvFileByDebug
	char  szFlvFile[256] = { 0 };
	sprintf(szFlvFile,".\\%X_%d.flv", this, rand());
	s_flv = flv_writer_create(szFlvFile);
#endif
#ifdef  WriteFlvToEsFileFlag
	char    szVideoFile[256] = { 0 };
	char    szAudioFile[256] = { 0 };
	sprintf(szVideoFile, ".\\%X_%d.264", this, rand());
	sprintf(szAudioFile, ".\\%X_%d.aac", this, rand());
	fWriteVideo = fopen(szVideoFile, "wb");
	fWriteAudio = fopen(szAudioFile, "wb");
#endif
	memset(&handler, 0, sizeof(handler));
	handler.send = rtmp_client_send;
	handler.onaudio = rtmp_client_onaudio;
	handler.onvideo = rtmp_client_onvideo;
	handler.onscript = rtmp_client_onscript;

	if (ParseRtspRtmpHttpURL(szIP) == true)
	 uint32_t ret = XHNetSDK_Connect((int8_t*)m_rtspStruct.szIP, atoi(m_rtspStruct.szPort), (int8_t*)(NULL), 0, (uint64_t*)&nClient, onread, onclose, onconnect, 0, 5000, 1);

	nVideoDTS = 0 ;
	nAudioDTS = 0 ;
	memset(szRtmpName, 0x00, sizeof(szRtmpName));
	netBaseNetType = NetBaseNetType_RtmpClientRecv;

	WriteLog(Log_Debug, "CNetClientRecvRtmp =%X 构造 nClient = %llu ",this, nClient);
}

CNetClientRecvRtmp::~CNetClientRecvRtmp()
{
	bRunFlag = false;
	std::lock_guard<std::mutex> lock(NetClientRecvRtmpLock);

	XHNetSDK_Disconnect(nClient);
	int nState;

	if (rtmp != NULL)
	{
		 nState = rtmp_client_getstate(rtmp);
		 if(nState >= 3)
		   rtmp_client_stop(rtmp);

		rtmp_client_destroy(rtmp);
	}

	if(flvDemuxer)
	  flv_demuxer_destroy(flvDemuxer);

#ifdef  WriteFlvFileByDebug
	flv_writer_destroy(s_flv);
#endif
#ifdef  WriteFlvToEsFileFlag
 	fclose(fWriteVideo);
	fclose(fWriteAudio);
#endif

	NetDataFifo.FreeFifo();
 
	WriteLog(Log_Debug, "CNetClientRecvRtmp = %X 析构 nClient = %llu \r\n", this, nClient);
}

int CNetClientRecvRtmp::PushVideo(uint8_t* pVideoData, uint32_t nDataLength, char* szVideoCodec)
{
	return 0;
}

int CNetClientRecvRtmp::PushAudio(uint8_t* pVideoData, uint32_t nDataLength, char* szAudioCodec, int nChannels, int SampleRate)
{
	return 0;
}
int CNetClientRecvRtmp::SendVideo()
{
	return 0;
}

int CNetClientRecvRtmp::SendAudio()
{

	return 0;
}

int CNetClientRecvRtmp::InputNetData(NETHANDLE nServerHandle, NETHANDLE nClientHandle, uint8_t* pData, uint32_t nDataLength)
{
	if (!bRunFlag)
		return -1;
	nRecvDataTimerBySecond = 0;
	NetDataFifo.push(pData, nDataLength);
	return 0;
}

int CNetClientRecvRtmp::ProcessNetData()
{
	std::lock_guard<std::mutex> lock(NetClientRecvRtmpLock);
	if (!bRunFlag)
		return -1;

 	unsigned char* pData = NULL;
	int            nLength;

	pData = NetDataFifo.pop(&nLength);
	if (pData != NULL && rtmp != NULL)
	{
		if (nLength > 0)
			rtmp_client_input(rtmp, pData, nLength);
 
 		NetDataFifo.pop_front();
	}
	return 0;
}

//从url里面获取app \ stream 
bool   CNetClientRecvRtmp::GetAppStreamByURL(char* app, char* stream)
{
	int nPos1, nPos2;
	if (memcmp(szClientIP, "rtmp://", 7) != 0)
		return false;
	std::string strURL = szClientIP;
	nPos1 = strURL.find("/", 8);
	if (nPos1 > 0)
	{
		nPos2 = strURL.find("/", nPos1 + 1);
		if (nPos2 > 0)
		{
			memcpy(app, szClientIP + nPos1 + 1, nPos2 - nPos1 - 1);
			memcpy(stream, szClientIP + nPos2 + 1, strlen(szClientIP) - nPos2 - 1);
			return true;
		}
		else
			return false;
	}else 
	  return true;
}

//发送第一个请求
int CNetClientRecvRtmp::SendFirstRequst()
{
	char szApp[128] = { 0 }, szStream[256] = { 0 };
	if (!GetAppStreamByURL(szApp, szStream))
	{
		WriteLog(Log_Debug, "CNetClientRecvRtmp = %X 获取rtmp中的app、stream 有误 ,url = %s, nClient = %llu \r\n", this,szClientIP, nClient);
		DeleteNetRevcBaseClient(nClient);
		return -1;
	}

	//rtmp客户端连接，需要去掉第2级路径
	std::string strRtmpURL = szClientIP;
	int nPos = strRtmpURL.rfind("/", strlen(szClientIP));
	if (nPos <= 0)
	{
		WriteLog(Log_Debug, "CNetClientRecvRtmp = %X rtmp中的url有误 ,url = %s, nClient = %llu \r\n", this, szClientIP, nClient);
		pDisconnectBaseNetFifo.push((unsigned char*)&m_hParent, sizeof(m_hParent));

		DeleteNetRevcBaseClient(nClient);
		return -1;
	}
	szClientIP[nPos] = 0x00;

	flvDemuxer = flv_demuxer_create(NetRtmpClientRecvCallBackFLV, this);
	rtmp = rtmp_client_create(szApp, szStream, szClientIP, this, &handler);
	if (rtmp == NULL)
	{
		WriteLog(Log_Debug, "CNetClientRecvRtmp = %X rtmp连接失败 ,url = %s, nClient = %llu \r\n", this, szClientIP, nClient);
		DeleteNetRevcBaseClient(nClient);
		return -1;
	}
	int  r = rtmp_client_start(rtmp, 1);

	return 0;
}

//请求m3u8文件
bool  CNetClientRecvRtmp::RequestM3u8File()
{
	return true;
}