
#include "VideoCapture.h"
#include "RtspVideoCapture.h"
VideoCapture* VideoCapture::CreateVideoCapture(std::string videourl)
{
	std::map<std::string, std::string> opts;
	if ((videourl.find("rtsp://") == 0)|| (videourl.find("http://") == 0) 
		|| (videourl.find("file://") == 0) || (videourl.find("rtmp://") == 0))
	{	
	return new RtspVideoCapture(videourl, opts);
	}
	else if ((videourl.find("file://") == 0))
	{
		//return new RtspVideoCapture(videourl, opts);
	}
#if defined(WEBRTC_WIN) 
	else if ((videourl.find("screen://") == 0))
	{
		//return new MyDesktopCapture();
	}
#endif // WEBRTC_WIN
	else if ((videourl.find("window://") == 0))
	{
		
	}
	else
	{
		//return new VideoCaptureImpl();
	}
	return nullptr;

}