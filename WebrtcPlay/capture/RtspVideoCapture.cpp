#include "RtspVideoCapture.h"

#include "NetClientRecvRtsp.h"
#include "NetClientRecvFLV.h"
#include "NetClientRecvRtmp.h"
#include "NetClientRecvHttpHLS.h"
#include "NetClientRecvFMP4.h"


int nRecvCount = 0;
void  live555RTSP_AudioVideo(int nRtspChan, int RtspDataType, char* codeName, unsigned char* pAVData, int nAVDataLength, int64_t timeValue, void* pCustomerData)
{
	nRecvCount++;
	
	printf("nRtspChan = %d,  RtspDataType =%d,  codeName = %s, nAVDataLength = %d \r\n", nRtspChan, RtspDataType, codeName, nAVDataLength);


	if (RtspDataType == XHRtspDataType_Message)
	{//收到消息通知 
		if (strcmp(codeName, "success") == 0)
		{//连接成功，会返回媒体信息  {"video":"H264","audio":"AAC","channels":1,"sampleRate":16000} ，可以确定媒体格式 
			printf("%s \r\n", pAVData);
		}
		else if (strcmp(codeName, "error") == 0)
		{//连接失败
			printf("%s \r\n", pAVData);
		}
	}
	else if (RtspDataType == XHRtspDataType_Video)
	{//收到回调视频
		RtspVideoCapture* capture = static_cast<RtspVideoCapture *>(pCustomerData);
		capture->onData("h264", pAVData, nAVDataLength,timeValue);
		//可以进行解码、或者保存数据
	}
	else if (RtspDataType == XHRtspDataType_Audio)
	{//收到回调音频
	   //可以进行解码、或者保存数据
	}

#ifdef WriteMyMp4FileFlag
	if (pAVData[4] == 0x67)
	{//记录SPS PPS 
		if (bRecordSPSPPSFlag == false)
		{
			memcpy(SPSPPSBuffer, pAVData, nAVDataLength);
			nSPSPPSLength = nAVDataLength;
			bRecordSPSPPSFlag = true;
		}
	}


	if (RtspDataType == XHRtspDataType_Video)
	{
		int nLengthCount;
		if (pAVData[4] == 0x65)
		{
			nLengthCount = nSPSPPSLength + nAVDataLength;
			fwrite((char*)&nLengthCount, 1, sizeof(int), fMyFile);
			fwrite((char*)SPSPPSBuffer, 1, nSPSPPSLength, fMyFile);

			fwrite((char*)pAVData, 1, nAVDataLength, fMyFile);
			fflush(fMyFile);
		}
	}
#endif

#ifdef WriteRtspDataFlag
	if (RtspDataType == XHRtspDataType_Video || RtspDataType == XHRtspDataType_Audio)
	{
		mediaHead.mediaDataLength = nAVDataLength;
		if (RtspDataType == XHRtspDataType_Video)
		{
			if (strcmp(codeName, "H264") == 0)
				mediaHead.mediaDataType = MedisDataType_H264;
			else if (strcmp(codeName, "H265") == 0)
				mediaHead.mediaDataType = MedisDataType_H265;
		}
		else if (RtspDataType == XHRtspDataType_Audio)
		{
			if (strcmp(codeName, "G711_A") == 0)
				mediaHead.mediaDataType = MedisDataType_G711A;
			else if (strcmp(codeName, "G711_U") == 0)
				mediaHead.mediaDataType = MedisDataType_G711U;
			else if (strcmp(codeName, "AAC") == 0)
				mediaHead.mediaDataType = MedisDataType_AAC;
		}

		fwrite((char*)&mediaHead, 1, sizeof(mediaHead), fMediaFile);
		fwrite(pAVData, 1, nAVDataLength, fMediaFile);
		fflush(fMediaFile);
	}
#endif


}

RtspVideoCapture::RtspVideoCapture(const std::string& uri, const std::map<std::string, std::string>& opts)
{
	int   nRtspChan[512] = { 0 };
	live555Client_ConnectCallBack((char *)uri.c_str(), XHRtspURLType_Liveing, true, this, live555RTSP_AudioVideo, nRtspChan[0]);
	//m_bStop.store(false);
	//char szIP[512] = { 0 };
	//char szShareMediaURL[512] = { 0 };
	//strcpy(szIP, uri.c_str());
	//unsigned short nPort = 0;
	//void* pCustomerPtr = NULL;
	//uint64_t hParent = 0;
	//int nXHRtspURLType = XHRtspURLType_Liveing;
	//if (memcmp(szIP, "http://", 7) == 0 && strstr(szIP, ".m3u8") != NULL)
	//{//hls 暂时不支持 hls 拉流 
	//	//在构造函数进行异步连接，会产生一个nClient值 
	//	pXHClient = std::make_shared<CNetClientRecvHttpHLS>(serverHandle, CltHandle, szIP, nPort, (char*)szShareMediaURL, pCustomerPtr, live555RTSP_AudioVideo, hParent, nXHRtspURLType);
	//	CltHandle = pXHClient->nClient; //把nClient赋值给 CltHandle ,作为关键字 ，如果连接失败，会收到回调通知，在回调通知进行删除即可 
	//}
	//else if (memcmp(szIP, "rtsp://", 7) == 0)
	//{//rtsp 
	//	pXHClient = std::make_shared<CNetClientRecvRtsp>(serverHandle, CltHandle, szIP, nPort, (char*)szShareMediaURL, pCustomerPtr, live555RTSP_AudioVideo, hParent, nXHRtspURLType);
	//	CltHandle = pXHClient->nClient; //把nClient赋值给 CltHandle ,作为关键字 ，如果连接失败，会收到回调通知，在回调通知进行删除即可 
	//
	//}
	//else if (memcmp(szIP, "http://", 7) == 0 && strstr(szIP, ".flv") != NULL)
	//{//flv 
	//	pXHClient = std::make_shared<CNetClientRecvFLV>(serverHandle, CltHandle, szIP, nPort, (char*)szShareMediaURL, pCustomerPtr, live555RTSP_AudioVideo, hParent, nXHRtspURLType);
	//	CltHandle = pXHClient->nClient; //把nClient赋值给 CltHandle ,作为关键字 ，如果连接失败，会收到回调通知，在回调通知进行删除即可 
	//}
	//else if (memcmp(szIP, "rtmp://", 7) == 0)
	//{//rtmp
	//	pXHClient = std::make_shared<CNetClientRecvRtmp>(serverHandle, CltHandle, szIP, nPort, (char*)szShareMediaURL, pCustomerPtr, live555RTSP_AudioVideo, hParent, nXHRtspURLType);
	//	CltHandle = pXHClient->nClient; //把nClient赋值给 CltHandle ,作为关键字 ，如果连接失败，会收到回调通知，在回调通知进行删除即可 
	//}
	//else if ((memcmp(uri.c_str(), "http://", 7) == 0 && strstr(uri.c_str(), ".mp4") != NULL) || (memcmp(szIP, "http://", 7) == 0 && strstr(szIP, ".MP4") != NULL))
	//{//mp4 
	//	pXHClient = std::make_shared<CNetClientRecvFMP4>(serverHandle, CltHandle, szIP, nPort, (char*)szShareMediaURL, pCustomerPtr, live555RTSP_AudioVideo, hParent, nXHRtspURLType);
	//	CltHandle = pXHClient->nClient; //把nClient赋值给 CltHandle ,作为关键字 ，如果连接失败，会收到回调通知，在回调通知进行删除即可 
	//}
	//else
	//	return ;

	//if (CltHandle == 0)
	//{//连接失败
	//	RTC_LOG(LS_INFO) << "CreateNetRevcBaseClient";
	//	//RTC_LOG(Log_Debug, "CreateNetRevcBaseClient()，连接 rtsp 服务器 失败 szURL = %s , szIP = %s ,port = %d ", szIP, pXHClient->m_rtspStruct.szIP, pXHClient->m_rtspStruct.szPort);
	//	
	//}

}

RtspVideoCapture::~RtspVideoCapture()
{
	Destroy();
}

bool RtspVideoCapture::Start()
{
	RTC_LOG(LS_INFO) << "LiveVideoSource::Start";
	m_capturethread = std::thread(&RtspVideoCapture::CaptureThread, this);
	return true;
}

void RtspVideoCapture::Destroy()
{
	RTC_LOG(LS_INFO) << "LiveVideoSource::stop";

	m_capturethread.join();
	m_YuvCallbackList.clear();

	Stop(NULL);
}

void RtspVideoCapture::Stop(VideoYuvCallBack yuvCallback)
{
	bool bEmpty = false;

	std::lock_guard<std::mutex> _lock(m_mutex);
	std::list<VideoYuvCallBack>::iterator it = m_YuvCallbackList.begin();
	while (it != m_YuvCallbackList.end())
	{
		if (it->target<void*>() == yuvCallback.target<void*>())
		{
			m_YuvCallbackList.erase(it);
			break;
		}
		it++;
	}
	if (m_YuvCallbackList.empty())
	{
		bEmpty = true;
	}

	if (bEmpty)
	{
	}
}

void RtspVideoCapture::Init(const char* devicename, int nWidth, int nHeight, int nFrameRate)
{
	
	m_nWidth = nWidth;
	m_nHeight = nHeight;
	m_nFrameRate = nFrameRate;
}

void RtspVideoCapture::Init(std::map<std::string, std::string> opts)
{
	if (opts.find("width") != opts.end())
	{
		m_nWidth = std::stoi(opts.at("width"));
	}
	if (opts.find("height") != opts.end()) {
		m_nHeight = std::stoi(opts.at("height"));
	}
	if (opts.find("fps") != opts.end()) {
		m_nFrameRate = std::stoi(opts.at("fps"));
	}
}

void RtspVideoCapture::RegisterCallback(VideoYuvCallBack yuvCallback)
{
	std::lock_guard<std::mutex> _lock(m_mutex);
	std::list<VideoYuvCallBack>::iterator it = m_YuvCallbackList.begin();
	while (it != m_YuvCallbackList.end())
	{
		if (it->target<void*>() == yuvCallback.target<void*>())
		{
			m_mutex.unlock();
			return;
		}
		it++;
	}
	m_YuvCallbackList.push_back(yuvCallback);
}

void RtspVideoCapture::CaptureThread()
{

}

bool RtspVideoCapture::onData(const char* id, unsigned char* buffer, int size, int64_t ts)
{
	m_h264Callback((char*)buffer, size, 0, 1280, 720, ts);

	return false;
}
