#pragma once

#include "VideoCapture.h"
#include <fstream>
#include <string.h>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>


#include "third_party/libyuv/include/libyuv.h"
#include "third_party/libyuv/include/libyuv/video_common.h"
#include "third_party/libyuv/include/libyuv/convert.h"

#include "media/base/codec.h"
#include "media/base/video_common.h"
#include "media/base/video_broadcaster.h"
#include "media/engine/internal_decoder_factory.h"

#include "common_video/h264/h264_common.h"
#include "common_video/h264/sps_parser.h"
#include "modules/video_coding/h264_sprop_parameter_sets.h"
#include "api/video_codecs/video_decoder.h"



#include "rtc_base/thread.h"
#include "media/base//adapted_video_track_source.h"
#include "pc/video_track_source.h"
#include "api/video/i420_buffer.h"
#include "modules/desktop_capture/desktop_capturer.h"
#include "modules/desktop_capture/desktop_capture_options.h"
#include "modules/video_capture/video_capture.h"
#include "modules/video_capture/video_capture_factory.h"



#include "NetRecvBase.h"
#include "XHNetSDK.h"
class RtspVideoCapture :public VideoCapture
{


public:
	RtspVideoCapture(const std::string& uri, const std::map<std::string, std::string>& opts = {});

	~RtspVideoCapture();


	/*VideoCapture callback*/
	virtual bool Start();

	virtual void Destroy();

	virtual void Stop(VideoYuvCallBack yuvCallback);

	virtual void Init(const char* devicename, int nWidth, int nHeight, int nFrameRate);

	virtual void Init(std::map<std::string, std::string> opts = {});

	virtual void RegisterCallback(VideoYuvCallBack yuvCallback);

	virtual void RegisterH264Callback(H264CallBack h264Callback) { m_h264Callback = h264Callback; };

	void CaptureThread();

	bool onData(const char* id, unsigned char* buffer, int size,  int64_t ts);




private:
		char        m_stop;

private:

	int m_nWidth = 1920;
	int m_nHeight = 1080;
	int m_nFrameRate = 30;


	int64_t next_timestamp_us_ = rtc::kNumMicrosecsPerMillisec;
	std::atomic<bool>m_bStop ;
	std::mutex m_mutex;                //������		


	std::list<VideoYuvCallBack> m_YuvCallbackList;

	H264CallBack m_h264Callback;
	std::thread                        m_capturethread;
	cricket::VideoFormat               m_format;
	std::vector<uint8_t>               m_cfg;
	std::map<std::string, std::string> m_codec;


	std::shared_ptr<CNetRevcBase> pXHClient;
	NETHANDLE CltHandle;
	NETHANDLE serverHandle;
};

